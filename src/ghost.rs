#[derive(Clone, Debug)]
pub struct Ghost {
    pub position: (i32, i32),
    pub ghost_type: GhostType,
    pub dead: bool,
}

#[derive(Clone, Debug)]
pub enum GhostType {
    Blue,
    Red,
}

impl Ghost {
    pub fn new(x: i32, y: i32, ghost_type: GhostType) -> Ghost {
        Ghost {
            position: (x, y),
            ghost_type,
            dead: false,
        }
    }

    pub fn from(x: i32, y: i32, ch: char) -> Option<Ghost> {
        match ch {
            '1' => Some(Ghost::new(x, y, GhostType::Blue)),
            '2' => Some(Ghost::new(x, y, GhostType::Red)),
            _ => None,
        }
    }

    pub fn to_char(&self) -> char {
        match self.ghost_type {
            GhostType::Blue => '1',
            GhostType::Red => '2',
        }
    }
}
