use std::collections::VecDeque;

use crate::all_levels::LEVELS;
use crate::direction::Direction;
use crate::item::Item;
use crate::level::Level;

pub struct Model {
    pub level: Level,
    pub level_frame: usize,
    pub level_num: usize,
    pub pill_counter: usize,
    pub player_frame: i32,
    pub player_frame_dir: i32,
    pub player_state: PlayerState,
    pub lives: usize,
    pub captures: usize,
    pub queued_input: VecDeque<Direction>,
    pub previous_score: usize,
    pub level_score: usize,
    pub high_score: usize,
}

impl Model {
    pub fn new() -> Model {
        let level_num = 0;
        Model {
            level: LEVELS[level_num].clone(),
            level_frame: 0,
            level_num,
            pill_counter: 0,
            player_frame: 1,
            player_frame_dir: 1,
            player_state: PlayerState::Playing,
            lives: 3,
            captures: 0,
            queued_input: VecDeque::new(),
            level_score: 0,
            previous_score: 0,
            high_score: 0,
        }
    }

    pub fn has_any_apples(&self) -> bool {
        self.level
            .grid
            .iter()
            .any(|row| row.iter().any(|item| *item == Item::Apple))
    }

    pub fn ghost_at(&self, x: i32, y: i32) -> bool {
        self.level
            .ghosts
            .iter()
            .any(|ghost| ghost.position == (x, y))
    }

    /// If there is a non-dead ghost at x, y, mark it as dead and return true.
    /// If there is no ghost there, or it's already dead, return false.
    pub fn kill_ghost_at(&mut self, x: i32, y: i32) -> bool {
        self.level
            .ghosts
            .iter_mut()
            .find(|ghost| ghost.position == (x, y))
            .map(|ghost| {
                if !ghost.dead {
                    ghost.dead = true;
                    true
                } else {
                    false
                }
            })
            .unwrap_or(false)
    }

    pub fn go_to_level(&mut self, level_num: usize) {
        self.level_num = level_num;
        self.level = LEVELS[level_num].clone();
        self.pill_counter = 0;
        self.player_state = PlayerState::Playing;
        self.level_frame = 0;
        self.level_score = 0;
        self.player_frame = 1;
        self.player_frame_dir = 1;
        self.queued_input.clear();
    }
}

pub enum PlayerState {
    Complete,
    Dead,
    Lost,
    Playing,
    Won,
}
