use once_cell::sync::Lazy;

pub static IMAGES: Lazy<Images> = Lazy::new(|| Images::new());

pub const PIXELS_PER_SQUARE: i32 = 17;
pub const NUM_PLAYER_FRAMES: usize = 4;
pub const LIVES_PIXELS: i32 = 9;
pub const VICTORY_PIXELS: usize = 150;

pub struct Image {
    pub width: u32,
    pub height: u32,
    pub bytes: Vec<u8>,
}

impl Image {
    fn from(img: image::DynamicImage) -> Image {
        if let image::DynamicImage::ImageRgba8(img) = img {
            Image {
                width: img.width(),
                height: img.height(),
                bytes: img.into_vec(),
            }
        } else {
            panic!("Unexpected pixel format.");
        }
    }
}

pub struct PlayerImages {
    pub victory: [Image; NUM_PLAYER_FRAMES],
    pub up: [Image; NUM_PLAYER_FRAMES],
    pub down: [Image; NUM_PLAYER_FRAMES],
    pub left: [Image; NUM_PLAYER_FRAMES],
    pub right: [Image; NUM_PLAYER_FRAMES],
}

impl PlayerImages {
    fn new() -> PlayerImages {
        let player_01 = decode(include_bytes!("png/player-01.png"));
        let player_02 = decode(include_bytes!("png/player-02.png"));
        let player_03 = decode(include_bytes!("png/player-03.png"));
        let player_04 = decode(include_bytes!("png/player-04.png"));
        let filter = image::imageops::FilterType::Nearest;
        let sz = VICTORY_PIXELS as u32;
        PlayerImages {
            victory: [
                Image::from(player_01.resize(sz, sz, filter)),
                Image::from(player_02.resize(sz, sz, filter)),
                Image::from(player_03.resize(sz, sz, filter)),
                Image::from(player_04.resize(sz, sz, filter)),
            ],
            up: [
                Image::from(player_01.rotate270()),
                Image::from(player_02.rotate270()),
                Image::from(player_03.rotate270()),
                Image::from(player_04.rotate270()),
            ],
            down: [
                Image::from(player_01.rotate90()),
                Image::from(player_02.rotate90()),
                Image::from(player_03.rotate90()),
                Image::from(player_04.rotate90()),
            ],
            left: [
                Image::from(player_01.fliph()),
                Image::from(player_02.fliph()),
                Image::from(player_03.fliph()),
                Image::from(player_04.fliph()),
            ],
            right: [
                Image::from(player_01),
                Image::from(player_02),
                Image::from(player_03),
                Image::from(player_04),
            ],
        }
    }
}

pub struct Images {
    pub player: PlayerImages,
    pub apple: Image,
    pub ghost_blue: Image,
    pub ghost_dead: Image,
    pub ghost_frozen: Image,
    pub ghost_red: Image,
    pub capture: Image,
    pub life: Image,
    pub pill: Image,
    pub wall: Image,
    pub victory: Option<[Image; 4]>,
}

impl Images {
    fn new() -> Images {
        Images {
            player: PlayerImages::new(),
            apple: Image::from(decode(include_bytes!("png/apple.png"))),
            ghost_blue: Image::from(decode(include_bytes!(
                "png/ghost-blue.png"
            ))),
            ghost_dead: Image::from(decode(include_bytes!(
                "png/ghost-dead.png"
            ))),
            ghost_frozen: Image::from(decode(include_bytes!(
                "png/ghost-frozen.png"
            ))),
            ghost_red: Image::from(decode(include_bytes!("png/ghost-red.png"))),
            capture: Image::from(decode(include_bytes!("png/capture.png"))),
            life: Image::from(decode(include_bytes!("png/life.png"))),
            pill: Image::from(decode(include_bytes!("png/pill.png"))),
            wall: Image::from(decode(include_bytes!("png/wall.png"))),
            victory: None,
        }
    }
}

fn decode(bytes: &[u8]) -> image::DynamicImage {
    image::load_from_memory_with_format(bytes, image::ImageFormat::Png)
        .expect("Unable to decode as PNG.")
}
