use crate::ghost::Ghost;
use crate::item::Item;
use crate::level::Level;
use crate::player::Player;

pub fn level_to_string(level: &Level) -> String {
    let mut locations: Vec<Vec<Location>> = level
        .grid
        .iter()
        .map(|line| line.iter().map(|item| Location::of_item(*item)).collect())
        .collect();

    let p = &level.player;
    locations[p.position.1 as usize][p.position.0 as usize]
        .sprites
        .push(Sprite::Player(p.clone()));

    for g in &level.ghosts {
        locations[g.position.1 as usize][g.position.0 as usize]
            .sprites
            .push(Sprite::Ghost(g.clone()));
    }

    let mut commands = vec![];

    if level.name.len() > 0 {
        commands.push(Command::Name(String::from(&level.name)))
    }

    locations
        .into_iter()
        .map(|line| line_to_chars(line, &mut commands))
        .collect::<Vec<String>>()
        .into_iter()
        .chain(commands.iter().map(Command::to_string))
        .collect::<Vec<String>>()
        .join("\n")
}

fn line_to_chars(
    line: Vec<Location>,
    mut commands: &mut Vec<Command>,
) -> String {
    line.into_iter()
        .map(|loc| loc.into_char_plus_commands(&mut commands))
        .collect()
}

pub fn string_to_level(lines: &str) -> Level {
    let trimmed_lines = trim_lines(lines);
    let commands = parse_commands(&trimmed_lines);
    let locations = parse_locations(lines, &trimmed_lines, &commands);
    let grid = grid_from_locations(&locations);
    let player = player_from_locations(&locations);
    let ghosts = ghosts_from_locations(&locations);
    let name = name_from_commands(&commands);

    Level {
        name,
        grid,
        player,
        ghosts,
    }
}

fn grid_from_locations(locations: &Vec<Vec<Location>>) -> Vec<Vec<Item>> {
    locations
        .iter()
        .map(|line| line.iter().map(|location| location.item).collect())
        .collect()
}

fn player_from_locations(locations: &Vec<Vec<Location>>) -> Player {
    let mut player = None;
    for line in locations {
        for location in line {
            for sprite in &location.sprites {
                if let Sprite::Player(p) = sprite {
                    if let Some(_) = player {
                        panic!("Multiple players!")
                    } else {
                        player = Some(p)
                    }
                }
            }
        }
    }

    if let Some(player) = player {
        player.clone()
    } else {
        panic!("No player position in level!");
    }
}

fn ghosts_from_locations(locations: &Vec<Vec<Location>>) -> Vec<Ghost> {
    let mut ghosts = vec![];
    for line in locations {
        for location in line {
            for sprite in &location.sprites {
                if let Sprite::Ghost(g) = sprite {
                    ghosts.push(g.clone())
                }
            }
        }
    }

    ghosts
}

#[derive(Clone, Debug)]
enum Sprite {
    Player(Player),
    Ghost(Ghost),
}

impl Sprite {
    fn from(x: i32, y: i32, ch: char) -> Option<Sprite> {
        if let Some(player) = Player::from(x, y, ch) {
            Some(Sprite::Player(player))
        } else if let Some(ghost) = Ghost::from(x, y, ch) {
            Some(Sprite::Ghost(ghost))
        } else if ch == '*' {
            None
        } else {
            panic!("Unrecognised character {}", ch)
        }
    }

    fn to_char(&self) -> char {
        match self {
            Sprite::Player(player) => player.to_char(),
            Sprite::Ghost(ghost) => ghost.to_char(),
        }
    }

    fn set_position(&mut self, x: i32, y: i32) {
        match self {
            Sprite::Player(player) => player.position = (x, y),
            Sprite::Ghost(ghost) => ghost.position = (x, y),
        }
    }
}

#[derive(Clone)]
struct Location {
    item: Item,
    sprites: Vec<Sprite>,
}

impl Location {
    fn of_sprite(sprite: Sprite) -> Location {
        Location {
            item: Item::Space,
            sprites: vec![sprite],
        }
    }

    fn of_item(item: Item) -> Location {
        Location {
            item: item,
            sprites: vec![],
        }
    }

    fn into_char_plus_commands(self, commands: &mut Vec<Command>) -> char {
        let mut num_things = self.sprites.len();
        if self.item != Item::Space {
            num_things += 1;
        }
        if num_things >= 2 {
            commands.push(Command::Location(Location {
                item: self.item,
                sprites: self.sprites,
            }));
            '*'
        } else {
            if self.item == Item::Space && self.sprites.len() == 1 {
                self.sprites[0].to_char()
            } else {
                self.item.to_char()
            }
        }
    }

    fn to_string(&self) -> String {
        let mut chars = String::from("");
        if self.item != Item::Space {
            chars.push(self.item.to_char());
        }
        for sprite in &self.sprites {
            chars.push(sprite.to_char());
        }
        chars
    }
}

enum MaybeResolvedLocation {
    Location(Location),
    UnresolvedLocation,
}

impl MaybeResolvedLocation {
    fn from(x: i32, y: i32, ch: char) -> MaybeResolvedLocation {
        if let Some(item) = Item::from(ch) {
            MaybeResolvedLocation::Location(Location::of_item(item))
        } else if let Some(sprite) = Sprite::from(x, y, ch) {
            MaybeResolvedLocation::Location(Location::of_sprite(sprite))
        } else {
            MaybeResolvedLocation::UnresolvedLocation
        }
    }
}

fn is_grid_line(line: &str) -> bool {
    line.chars().next().map(|ch| ch != ':').unwrap_or(false)
}

enum Command {
    Location(Location),
    Name(String),
}

impl Command {
    fn from(line: &str) -> Command {
        if &line[0..1] != ":" {
            panic!(
                "Property lines should start with ':' but this line is '{}'.",
                line
            );
        }

        let mut spl = line[1..].splitn(2, '=');
        if let Some(name) = spl.next() {
            if let Some(value) = spl.next() {
                match name {
                    "*" => star_command(value, line),
                    "name" => name_command(value),
                    _ => {
                        panic!("Unknown command '{}' in line '{}'", name, line)
                    }
                }
            } else {
                panic!("Property line '{}' contains no '='.", line);
            }
        } else {
            panic!("Invalid property line '{}'.", line);
        }
    }

    fn to_string(&self) -> String {
        match self {
            Command::Location(loc) => format!(":*={}", loc.to_string()),
            Command::Name(name) => format!(":name={}", name),
        }
    }
}

fn name_command(name: &str) -> Command {
    Command::Name(String::from(name))
}

fn star_command(chars: &str, line: &str) -> Command {
    let mut item = None;
    let mut sprites = vec![];

    for ch in chars.chars() {
        if let Some(i) = Item::from(ch) {
            if let Some(_) = item {
                panic!("Two static items on same square in line {}!", line);
            } else {
                item = Some(i);
            }
        } else if let Some(sprite) = Sprite::from(-1, -1, ch) {
            sprites.push(sprite);
        } else {
            panic!("Unknown character '{}' in line '{}'", ch, line)
        }
    }

    Command::Location(Location {
        item: item.unwrap_or(Item::Space),
        sprites,
    })
}

fn trim_lines(lines: &str) -> Vec<&str> {
    lines.trim().lines().map(|line| line.trim()).collect()
}

fn parse_commands(trimmed_lines: &Vec<&str>) -> Vec<Command> {
    trimmed_lines
        .iter()
        .filter(|&line| !is_grid_line(*line))
        .map(|&line| Command::from(line))
        .collect()
}

fn parse_locations(
    lines: &str,
    trimmed_lines: &Vec<&str>,
    commands: &Vec<Command>,
) -> Vec<Vec<Location>> {
    let locations: Vec<Vec<MaybeResolvedLocation>> = trimmed_lines
        .iter()
        .filter(|&line| is_grid_line(*line))
        .map(|line| {
            line.chars()
                .map(|ch| MaybeResolvedLocation::from(-1, -1, ch))
                .collect()
        })
        .collect();

    if locations.len() == 0 {
        panic!("Creating a level with no lines:\n{}", lines);
    }
    let width = locations[0].len();
    if width == 0 {
        panic!("Creating a level with a zero-length line:\n{}", lines);
    }

    for line in &locations {
        if line.len() != width {
            panic!("Inconsistent line lengths in level:\n{}", lines);
        }
    }

    let mut extra_locations = commands.iter().filter_map(|cmd| {
        if let Command::Location(loc) = cmd {
            Some(loc)
        } else {
            None
        }
    });

    let ret = locations
        .into_iter()
        .enumerate()
        .map(|(y, row)| {
            row.into_iter()
                .map(|loc| match loc {
                    MaybeResolvedLocation::Location(loc) => loc,
                    MaybeResolvedLocation::UnresolvedLocation => {
                        match extra_locations.next() {
                            Some(loc) => loc.clone(),
                            None => panic!("Not enough :* lines in level!"),
                        }
                    }
                })
                .enumerate()
                .map(|(x, mut loc)| {
                    for sprite in &mut loc.sprites {
                        sprite.set_position(x as i32, y as i32);
                    }
                    loc
                })
                .collect()
        })
        .collect();

    if let Some(_) = extra_locations.next() {
        panic!("Too many :* lines in level!")
    }

    ret
}

fn name_from_commands(commands: &Vec<Command>) -> String {
    commands
        .iter()
        .filter_map(|cmd| {
            if let Command::Name(name) = cmd {
                Some(String::from(name))
            } else {
                None
            }
        })
        .next()
        .unwrap_or(String::from(""))
}
